# Marine Microkiné

The goal is to make a website for my sister's company. The goal of this project is to make a state of the art, SEO-ready website.

My choices went for:

- Astro, as a modern static site builder
- Contentful, as CMS
- Hosted on Vercel
- Figma, to make early prototype: [Prototype](https://www.figma.com/proto/BV1KWmxp7qoNtc0YzxO5SL/Microkin%C3%A9-Landerneau?type=design&node-id=1302-1045&t=nxBkmrDLeUMNsg5v-0&scaling=scale-down&page-id=1302:1041&starting-point-node-id=1302:1045)

I'm fairly happy with the performances:

- Deployement take ~15s
- Core Web Vitals measured by Vercel are satisfying:

![100% Real Experience Score](cwv.png)

Deployed at [https://microkine-landerneau.fr](https://microkine-landerneau.fr)

---

# Astro Starter Kit: Minimal

```
npm create astro@latest -- --template minimal
```

[![Open in StackBlitz](https://developer.stackblitz.com/img/open_in_stackblitz.svg)](https://stackblitz.com/github/withastro/astro/tree/latest/examples/minimal)
[![Open with CodeSandbox](https://assets.codesandbox.io/github/button-edit-lime.svg)](https://codesandbox.io/p/sandbox/github/withastro/astro/tree/latest/examples/minimal)
[![Open in GitHub Codespaces](https://github.com/codespaces/badge.svg)](https://codespaces.new/withastro/astro?devcontainer_path=.devcontainer/minimal/devcontainer.json)

> 🧑‍🚀 **Seasoned astronaut?** Delete this file. Have fun!

## 🚀 Project Structure

Inside of your Astro project, you'll see the following folders and files:

```
/
├── public/
├── src/
│   └── pages/
│       └── index.astro
└── package.json
```

Astro looks for `.astro` or `.md` files in the `src/pages/` directory. Each page is exposed as a route based on its file name.

There's nothing special about `src/components/`, but that's where we like to put any Astro/React/Vue/Svelte/Preact components.

Any static assets, like images, can be placed in the `public/` directory.

## 🧞 Commands

All commands are run from the root of the project, from a terminal:

| Command                | Action                                           |
| :--------------------- | :----------------------------------------------- |
| `npm install`          | Installs dependencies                            |
| `npm run dev`          | Starts local dev server at `localhost:3000`      |
| `npm run build`        | Build your production site to `./dist/`          |
| `npm run preview`      | Preview your build locally, before deploying     |
| `npm run astro ...`    | Run CLI commands like `astro add`, `astro check` |
| `npm run astro --help` | Get help using the Astro CLI                     |

## 👀 Want to learn more?

Feel free to check [our documentation](https://docs.astro.build) or jump into our [Discord server](https://astro.build/chat).
