import contentful from "contentful";

export const contentfulClient = contentful.createClient({
  space: import.meta.env.CONTENTFUL_SPACE_ID,
  accessToken:
    import.meta.env.VERCEL_ENV !== "production"
      ? import.meta.env.CONTENTFUL_PREVIEW_ACCESS_TOKEN
      : import.meta.env.CONTENTFUL_ACCESS_TOKEN,
  host:
    import.meta.env.VERCEL_ENV !== "production"
      ? "preview.contentful.com"
      : "cdn.contentful.com",
});
